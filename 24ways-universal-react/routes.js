import AppComponent from './components/app';
import IndexComponent from './components/index';
import OfferView from './components/offer';
import CounterView from './components/counter';

const routes = {
  path: '',
  component: AppComponent,
  childRoutes: [
    {
      path: '/',
      component: IndexComponent
    },
    {
      path: '/offer',
      component: OfferView
    },
    {
      path: '/counter',
      component: CounterView
    }
  ]
}

export { routes };