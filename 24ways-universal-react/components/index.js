import React from 'react';

export default class IndexComponent extends React.Component {
  render() {
    return (
      <div>
        <p>This is the index page</p><br />
        <blink>Search Engine friendly!</blink>
      </div>
    );
  }
}
