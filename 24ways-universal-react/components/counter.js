import React from 'react';

export default class CounterView extends React.Component {
  
  	constructor(props) {
  		super(props);
  		this.state = {
  			count: 0
  		}
  	}

  	increment() {
  		this.setState({
  			count: this.state.count + 1
  		})
  	}

  	decrement() {
  		this.setState({
  			count: this.state.count - 1
  		})
  	}

	  render() {
	    return (
	      <div>
	        <p>Counter App</p><br />
	        <blink>Search Engine friendly!</blink>

	        <div id="counter-application">
	        	<div id="count-ctnr">
	        		<span>{ this.state.count }</span>
	        	</div>
	        	<div>
	        		<a onClick={ () => this.increment() } href="#">up</a>
	        		<a onClick={ () => this.decrement() } href="#">down</a>
	        	</div>
	        </div>
	      </div>
	    );
	  }
}
