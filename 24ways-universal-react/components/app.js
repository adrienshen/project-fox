import React from 'react';
import { Link } from 'react-router';

export default class AppComponent extends React.Component {
  render() {
    return (
      <div>
        <h2>Welcome to my App</h2>
        <ul>
          <li><Link to='/'>Home</Link></li>
          <li><Link to='/offer'>Offers</Link></li>
          <li><Link to='/counter'>Offers</Link></li>
        </ul>
        { this.props.children }
      </div>
    );
  }
}
